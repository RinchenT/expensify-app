import auth from "../../reducers/auth";

test("should set uuid when login", () => {
  const action = {
    type: "LOGIN",
    uid: 1,
  };

  const state = auth({}, action);
  expect(state.uid).toBe(action.uid);
});

test("should clear uuid when logout", () => {
  const action = {
    type: "LOGOUT",
  };
  const state = auth({ uid: "1" }, action);
  expect(state.uid).toBeUndefined();
});

export default (state = {}, action) => {
  switch (action.type) {
    case "LOGIN":
      return { uid: action.uid };
    case "LOGOUT":
      return {};
    default:
      return state;
  }
};
